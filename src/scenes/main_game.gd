extends Control

@onready var output = $Label
@onready var word_button = $VBoxContainer/word_btn
@onready var next_timer = $Next_Timer
var game_active = true
var movement = true
var item_number = 0

var pitch: float = 0.0
var roll: float = 0.0
var yaw: float = 0.0

func _ready():
	randomize()
	Words.actions.shuffle()

func _physics_process(delta):
	sensors(delta)
	
func next_word():
	$next.play(0)
	item_number += 2
	if item_number >= Words.actions.size():
		item_number = 0
		
	word_button.theme = load("res://themes/button_blue.tres")
	word_button.text = Words.actions[item_number].to_upper()
	if Words.actions[item_number] == "":
		next_word()


func sensors(delta):
	if !game_active:
		return
		
	var gravity: Vector3 = Input.get_gravity()
	if gravity == Vector3(0,0,0):
		return
		
	var r = Vector3()
	roll = atan2(-gravity.x, -gravity.y) 
	gravity = gravity.rotated(-Vector3.FORWARD, r.z)
	pitch = atan2(gravity.z, -gravity.y)
	
	r = Vector3(pitch, yaw, roll)
	output.text = str(r)# + "\n" + str(roll) +"\n" + str(yaw)
	
	if pitch > 1.5 && movement:
		$point.play(0)
		movement = false
		Global.score += 1
		word_button.text = "GOOD JOB!!!"
		word_button.theme = load("res://themes/button_green.tres")
		next_timer.start(0)
	elif pitch < -1 && movement:
		$pass.play(0)
		movement = false
		word_button.text = "-PASS-"
		word_button.theme = load("res://themes/button_orange.tres")
		next_timer.start(0)
	elif pitch < 1.2 && pitch > -.5 && !movement:
		movement = true

func count_down():
	$AnimationPlayer.play("timeup")

func time_up():
	game_active = false
	word_button.disabled = true
	word_button.text = "Time Up"
	word_button.theme = load("res://themes/button_red.tres")

func end_round():
	word_button.text = "Score: " + str(Global.score)
	
func _on_exit_btn_pressed() -> void:
	get_tree().quit()


func _on_reset_btn_pressed() -> void:
	get_tree().reload_current_scene()
