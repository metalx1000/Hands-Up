#!/bin/bash
######################################################################
#Copyright (C) 2024  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
######################################################################

sfile="src/scripts/words.gd"
echo -e "extends Node\n" >"$sfile"

find src/words/ -iname "*.lst" | while read file; do
	catagory="$(basename $file | cut -d\. -f1)"
	echo "var ${catagory} = ["
	cat "$file" | tr -d '"' | sed 's/\(.*\)/\t"\1",/g'
	echo -e "]\n"
done >>"$sfile"
